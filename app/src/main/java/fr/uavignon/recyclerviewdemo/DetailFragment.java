package fr.uavignon.recyclerviewdemo;

import android.app.Activity;
import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import fr.uavignon.recyclerviewdemo.data.Country;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.navigation.fragment.NavHostFragment;

import java.net.URI;

import static java.net.URI.*;

public class DetailFragment extends Fragment {

    public static final String TAG = "SecondFragment";

    TextView textView;
    TextView textView1;
    TextView textView2;
    TextView textView3;
    TextView textView4;
    TextView textView5;
    ImageView img;
    private Country[] t = Country.countries ;

    @Override
    public View onCreateView(
            LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState
    ) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_second, container, false);
    }

    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        textView = view.findViewById(R.id.textView3);
      textView1 = view.findViewById(R.id.textview_second);
       textView2 = view.findViewById(R.id.textView2);
       textView3 = view.findViewById(R.id.textView6);
       textView4 = view.findViewById(R.id.textView4);
        textView5 = view.findViewById(R.id.textView5);
        // Implementation with bundle
       img = view.findViewById(R.id.imageView2);



        DetailFragmentArgs args = DetailFragmentArgs.fromBundle(getArguments());
        textView.setText( Country.countries[args.getCountryId()].getName());
        textView1.setText(Country.countries[args.getCountryId()].getCapital());
        textView2.setText( Country.countries[args.getCountryId()].getLanguage());
        textView4.setText(Country.countries[args.getCountryId()].getCurrency());
        textView3.setText(Country.countries[args.getCountryId()].getArea() + " km²");
        textView5.setText(""+Country.countries[args.getCountryId()].getPopulation());


        String uri = t[args.getCountryId()].getImgUri();
        Context c = img.getContext();
        img.setImageDrawable(c.getResources().getDrawable(
                c.getResources(). getIdentifier (uri, null , c.getPackageName())));




        view.findViewById(R.id.button_second).setOnClickListener(new  View.OnClickListener() {
            @Override
            public void onClick(View view) {
                NavHostFragment.findNavController(DetailFragment.this)
                        .navigate(R.id.action_SecondFragment_to_FirstFragment);

            }
        });
    }
}