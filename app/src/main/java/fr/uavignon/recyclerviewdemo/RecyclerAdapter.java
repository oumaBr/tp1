package fr.uavignon.recyclerviewdemo;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.navigation.Navigation;
import androidx.recyclerview.widget.RecyclerView;

import fr.uavignon.recyclerviewdemo.data.Country;

public class RecyclerAdapter extends RecyclerView.Adapter<RecyclerAdapter.ViewHolder> {


    private fr.uavignon.recyclerviewdemo.data.Country Country;

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.card_layout, viewGroup, false);
        ViewHolder viewHolder = new ViewHolder(v);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int i) {

        viewHolder.name.setText(Country.countries[i].getName());
        viewHolder.capital.setText(Country.countries[i].getCapital());
        String uri = Country.countries[i].getImgUri();
        Context c = viewHolder.drapeau.getContext();
        viewHolder.drapeau.setImageDrawable(c.getResources().getDrawable(c.getResources().
                getIdentifier (uri , null , c.getPackageName())));


    }

    @Override
    public int getItemCount() {

        return Country.countries.length;
    }


    static class ViewHolder extends RecyclerView.ViewHolder {
        TextView name;
        TextView capital;
        ImageView  drapeau;

        ViewHolder(View itemView) {
            super(itemView);
            name = itemView.findViewById(R.id.item_title);
            capital = itemView.findViewById(R.id.item_detail);
            drapeau = itemView.findViewById(R.id.item_image);


            itemView.setOnClickListener(new View.OnClickListener() {
                @Override public void onClick(View v) {

                    int position = getAdapterPosition();

                    ListFragmentDirections.ActionFirstFragmentToSecondFragment action = ListFragmentDirections.actionFirstFragmentToSecondFragment();
                    action.setCountryId(position);
                    Navigation.findNavController(v).navigate(action);
                }
            });

        }
    }

}

